"""
    - Enunciat: Generar línies ".bed" a partir de dos arxius (genbank, fitxa). Utilitzeu la coordenada d'inici del gen de la fitxa.
    - Input: Llegiu un arxiu genbank i un arxiu en text pla que és la fitxa d'un gen. Per fer proves utilitzeu els dos arxius adjunts.
    - Output: Genereu una línia ".bed" per enganxar en el "custom track" del navegador genòmic del UCSC.
"""

import argparse
from Bio.SeqRecord import SeqRecord
from Bio import SeqFeature
from Bio import SeqIO
from Bio.Alphabet import IUPAC
import re

genbank = ""

"""
PARSER = argparse.ArgumentParser()
#PARSER.add_argument("genbank")
#ARGS = PARSER.parse_args()
#genbank = ARGS.genbank
"""

genbank = "insulin.gb"

with open("insulin.txt", "r") as txt_file:
    txt = txt_file.read()

# Seleccionamos una parte del fichero para ir acotando texto
# hasta llegar a los datos que quiero.


"""Acoto en todo el fichero desde GENOMIC CONTEXT hasta BIBLIOGRAPHY.  (En este tramo estan los numeros de comienzo y fin de los genes que necesito )"""
reg = r"GENOMIC CONTEXT(.*)BIBLIOGRAPHY"
pat = re.compile(reg, re.DOTALL)  
match = pat.search(txt)
match.group(1)

reg2 = r"(\d+)\.\.(\d+)"
pat2 = re.compile(reg2, re.DOTALL)
match2 = pat2.search(match.group(1))

start = int(match2.group(1))
end   = int(match2.group(2))

record = SeqIO.read("insulin.gb", "genbank")    # Guardamos la secuencia del gen mediante las funciones SeqIO    // Recuerda que SeqIO.read(..) para un elemento solo y SeqIO.parse para un multielementos
#print(record)                                         # Aqui se pueden ver los elementos que se han guardado en record
features = record.features                             # Guardamos la seccion features del genbank en la variable features

source_feature = record.features[0]
source_feature.qualifiers['map']                       # Aqui tenemos el cromosoma entre otras cosas y con regex aislamos el numero de cromosoma
localization_str = source_feature.qualifiers['map'][0]

reg3 = r"^\d\d"
pat3 = re.compile(reg3, re.DOTALL)
match3 = pat3.search(localization_str)

cromosoma = "Chr"+match3.group(0)+" "
lineal = cromosoma + str(start) +" " +str(end)

