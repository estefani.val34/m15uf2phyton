"""
MAIN , call other functions
"""

import argparse

import alignnw_lib

SECUENCIA1 = ""
SECUENCIA2 = ""

PARSER = argparse.ArgumentParser()
PARSER.add_argument("SECUENCIA1")
PARSER.add_argument("SECUENCIA2")
ARGS = PARSER.parse_args()

SECUENCIA1 = ARGS.SECUENCIA1
SECUENCIA2 = ARGS.SECUENCIA2
print("SECUENCIA1 = ", SECUENCIA1)
print("SECUENCIA2 = ", SECUENCIA2)

print("*** MATRIZ ***")
M = alignnw_lib.get_matrix(SECUENCIA1, SECUENCIA2)

print(M)
print("*** ALIGNMENT ***")
alignnw_lib.get_alignment(M, SECUENCIA1, SECUENCIA2)
