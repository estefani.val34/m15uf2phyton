
#Needleman–Wunsch algorithm
#https://en.wikipedia.org/wiki/Needleman%E2%80%93Wunsch_algorithm

"""
Program test wikipedia
"""

import alignnw_lib

SECUENCIA1 = "GCATGCU"
SECUENCIA2 = "GATTACA"

print("SECUENCIA1 = ", SECUENCIA1)
print("SECUENCIA2 = ", SECUENCIA2)

print("*** MATRIZ ***")
M = alignnw_lib.get_matrix(SECUENCIA1, SECUENCIA2)
print(M)

print("*** ALIGNMENT ***")
alignnw_lib.get_alignment(M, SECUENCIA1, SECUENCIA2)
