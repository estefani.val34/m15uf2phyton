#Smith–Waterman algorithm
#https://en.wikipedia.org/wiki/Smith%E2%80%93Waterman_algorithm

"""
Program test wikipedia
"""

import alignsw_lib

SECUENCIA1 = "TGTTACGG"
SECUENCIA2 = "GGTTGACTA"

print("SECUENCIA1 = ", SECUENCIA1)
print("SECUENCIA2 = ", SECUENCIA2)
print("*** MATRIZ ***")

M = alignsw_lib.get_matrix(SECUENCIA1, SECUENCIA2)
print(M)
INFO_SCORE = alignsw_lib.get_max_score(M)
print("*** MAX SCORE and POSITION ***")
print(INFO_SCORE)
print("*** ALIGNMENT ***")
alignsw_lib.get_alignment(M, SECUENCIA1, SECUENCIA2, INFO_SCORE)
