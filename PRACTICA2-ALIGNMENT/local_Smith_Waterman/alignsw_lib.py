#!/usr/bin/env python
"""
Program which recreates the Needleman Wunsch alignment.
    1.- Creates a Smith–Waterman algorithm matrix from two secuences inserted by user
    2.- Using the Smith–Waterman algorithm matrix, creates the alignment returned in 3 strings.
"""

import numpy as np


def get_matrix(secuencia1: str, secuencia2: str):
    """create the matrix of sequence 1 i of sequence 2-->matrix"""
    filas = len(secuencia2)
    columnas = len(secuencia1)
    matrix = np.zeros([filas+1, columnas+1], int)
    penalty = -2
    for j in range(columnas):
        for i in range(filas):
            letra2 = secuencia2[i]
            letra1 = secuencia1[j]
            if letra2 == letra1:
                concurrence = 3
            else:
                concurrence = -3
            fat = max(matrix[i, j]+concurrence, matrix[i+1, j]+penalty, matrix[i, j+1]+penalty, 0)
            matrix[i+1, j+1] = fat
    return matrix




def get_max_score(matrix):
    """returns the highest score of the matrix and its position-->tuple"""
    maxi = 0
    dimensiones = matrix.shape
    indexi = 0
    indexj = 0
    j = 0
    i = 0
    for j in range(dimensiones[1]):
        for i in range(dimensiones[0]):
            if matrix[i, j] > maxi:
                maxi = matrix[i, j]
                indexi = i
                indexj = j
    return maxi, indexi, indexj




def get_alignment(matrix: np.ndarray, secuencia1: str, secuencia2: str, info_maximo: tuple):
    """Return alignment--> 3 strings"""
    i = info_maximo[1]
    j = info_maximo[2]
    string1 = ""
    string2 = ""
    string3 = ""
    penalty = -2
    while(matrix[i, j] != 0):
        if secuencia2[i-1] == secuencia1[j-1]:
            concurrence = 3
        else:
            concurrence = -3
        if matrix[i, j] == matrix[i-1, j-1]+concurrence:
            string1 = string1+secuencia1[j-1]
            string2 = string2+secuencia2[i-1]
            string3 = string3+"|"
            i = i-1
            j = j-1
        elif matrix[i, j] == matrix[i-1, j]+penalty:
            string1 = string1+"-"
            string2 = string2+secuencia2[i-1]
            string3 = string3+" "
            i = i-1
        else:
            string1 = string1+secuencia1[j-1]
            string2 = string2+"-"
            string3 = string3+" "
            j = j-1
    print(string1[::-1])
    print(string3[::-1])
    print(string2[::-1])
