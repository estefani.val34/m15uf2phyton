"""Fasta"""

import sys
import argparse
import re


import local_Smith_Waterman.alignsw_lib
import global_Needleman_Wunsch.alignnw_lib

LIST_SEQ2 = []

SECUENCIA1 = ""
OPTION = ""
KEY_VALUE = {}


PARSER = argparse.ArgumentParser()
PARSER.add_argument("SECUENCIA1")
PARSER.add_argument('infile', nargs='?', type=argparse.FileType('r'), default=sys.stdin)

PARSER.add_argument("OPTION")
ARGS = PARSER.parse_args()

SECUENCIA1 = ARGS.SECUENCIA1.upper()
OPTION = ARGS.OPTION.upper()


F = open(ARGS.infile.name, "r")
#I have list of nitrogen bases

TXT = F.read()
REGEX = r'^>.*[^>]'

PAT = re.compile(REGEX, re.MULTILINE)
#matches = re.finditer(PAT, TXT)
TXT_SUB = PAT.sub("*", TXT)
LIST_TXT_SPLIT = TXT_SUB.split("*")
LENGHT = len(LIST_TXT_SPLIT)

for i in range(LENGHT):
    if LIST_TXT_SPLIT[i] != '':
        LIST_SEQ2.append(LIST_TXT_SPLIT[i].replace('\n', ''))

F.close()

print("OPTION=", OPTION)

if OPTION == "SW":
    for x in LIST_SEQ2:
        SECUENCIA2 = x
        M = local_Smith_Waterman.alignsw_lib.get_matrix(SECUENCIA1, SECUENCIA2)
        info_score = local_Smith_Waterman.alignsw_lib.get_max_score(M)
        KEY_VALUE[info_score[0]] = SECUENCIA2
    for i in sorted(KEY_VALUE, reverse=True):
        print("SECUENCIA1=", SECUENCIA1, " SECUENCIA2=", KEY_VALUE[i], "   Score=", i)
elif OPTION == "NW":
    for x in LIST_SEQ2:
        SECUENCIA2 = x
        M = global_Needleman_Wunsch.alignnw_lib.get_matrix(SECUENCIA1, SECUENCIA2)
        score = global_Needleman_Wunsch.alignnw_lib.get_max_score(M)
        KEY_VALUE[score] = SECUENCIA2
    for i in sorted(KEY_VALUE, reverse=True):
        print("SECUENCIA1=", SECUENCIA1, " SECUENCIA2=", KEY_VALUE[i], "   Score=", i)
else:
    raise ValueError("Enter and OPTION, please[sw/nw]")
