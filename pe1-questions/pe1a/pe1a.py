# - M15-UF2: (Bio)Python Pe1a (2019-12-16)
# ---------------------------------------------------------------------
# - Nom:
# - Cognoms:
# - DNI:
# ---------------------------------------------------------------------

"""
Problema A: Alineament (4 punts)
---------------------------------------------------------------------
Adjuntem el codi d'alineament global (Needleman-Wunsch).
Canvia el codi de forma que poguem espcificar diferents penalitzacions
a l'hora d'obrir un gap i a l'hora d'estendre un gap ja existent.
L'usuari especificarà aquestes penalitzacions a la línia d'ordres.

Exemple:
- Penalització per obrir un gap:    -5
- Penalització per estendre un gap: -2
- Línia d'ordres: python pe1a.py GATAGATA GAAA -5 -2

Tasques:
- Modifica args per tal de rebre dos paràmetres més: 'open_gap' i 'extend_gap'
- Canvia l'algorisme d'alineació per utilitzar aquests dos paràmetres
- Verifica que els teus resultats són com els de l'exemple.

Respostes:
- Escriu tot el teu codi en aquests dos fitxers: pe1a.py (aquest fitxer) i alignnw_lib.py
- Quan acabis, puja-ho tot al Moodle comprimit en un únic arxiu .zip.

Avaluació:
- Les vostres respostes s'avaluaran executant 'python pe1a.py GATAGATA GAAA -5 -2'
- Tots els fitxers estaran al mateix directori que pe1a.py.
- Assegureu-vos que el vostre codi NO dóna cap error abans d'entregar-lo!

Puntuació:
- Càlculs del gap:            1 punt
- Modificació de l'algorisme: 3 punts

Paquets requerits:
conda install -n bio -c anaconda pandas
"""

# ---------------------------------------------------------------------

import alignnw_lib as nw
import argparse

# ---------------------------------------------------------------------

# Parse cmdline
parser = argparse.ArgumentParser()
parser.add_argument("a")
parser.add_argument("b")
parser.add_argument("openGap")
parser.add_argument("extendGap")

# Get args
args = parser.parse_args()
a    = " " + args.a.upper()
b    = " " + args.b.upper()
openGap    = " " + args.openGap
extendGap    = " " + args.extendGap

# Make letter_scores dict
letter_scores = {}
letter_scores['same_letter'] =  3
letter_scores['diff_letter'] = -3
letter_scores['del_letter']  = -2
letter_scores['ins_letter']  = -2

# ---------------------------------------------------------------------
print("seq1: " + a)
print("seq2: " + b)

print("openGap: " + openGap)
print("extendGap: " + extendGap)

#print(type(openGap)) str

scores_mat, edits_mat, aligns_mat = nw.create_mats(a, b, letter_scores)

print("MATRICES:")
nw.print_matrix(a, b, scores_mat)
nw.print_matrix(a, b, edits_mat)
nw.print_matrix(a, b, aligns_mat)

a_alignment, b_alignment, edits, path_mat = nw.get_alignment(a, b, edits_mat, aligns_mat)
alignment = nw.print_alignment(a_alignment, b_alignment)

print("ALIGNMENT:")
nw.print_matrix(a, b, path_mat)
print(a_alignment)
print(alignment)
print(b_alignment)

# ---------------------------------------------------------------------

