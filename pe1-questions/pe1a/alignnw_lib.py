"""
Library for doing global alignments (Needleman-Wunsch).
"""

# Imports:
# ---------------------------------------------------------------------

import numpy as np
import pandas as pd

# ---------------------------------------------------------------------
def get_similarity(a_letter: str, b_letter: str, letter_scores: dict) -> (int):

    if a_letter == b_letter:
        return letter_scores['same_letter']
    else:
        return letter_scores['diff_letter']


# ---------------------------------------------------------------------
def get_score(a: str, b: str, i: int, j: int, scores_mat: np.ndarray, edits_mat: np.ndarray, letter_scores: dict ) -> (int, int, int):
    # Calculate all scores
    substitution_score = scores_mat[i-1, j-1] + get_similarity(a[i], b[j], letter_scores)
    deletion_score     = scores_mat[i-1, j  ] + letter_scores['del_letter']
    insertion_score    = scores_mat[i  , j-1] + letter_scores['ins_letter']

    # Pick best score, edit, align
    scores = [substitution_score, deletion_score, insertion_score]
    edits = ["s", "d", "i"]
    aligns = ["↖", "↑", "←"]

    max_score = max(scores)
    max_score_index = scores.index(max_score)

    best_score = max_score
    best_edit = edits[max_score_index]
    best_align = aligns[max_score_index]

    return best_score, best_edit, best_align


# ---------------------------------------------------------------------
def create_mats(a: str, b: str, letter_scores: dict) -> (np.ndarray, np.ndarray, np.ndarray):
   

    # Make scores matrix
    scores_mat = np.zeros((len(a), len(b)), int)
    for j in range(0, len(b)):
        scores_mat[0, j] = letter_scores['ins_letter'] * j

    for i in range(0, len(a)):
        scores_mat[i, 0] = letter_scores['del_letter'] * i

    # Make edits matrix
    edits_mat = np.zeros((len(a), len(b)), str)

    for j in range(0, len(b)):
        edits_mat[0, j] = "i"

    for i in range(0, len(a)):
        edits_mat[i, 0] = "d"
    edits_mat[0,0] = 'x'

    # Make aligns matrix
    aligns_mat = np.zeros((len(a), len(b)), str)
    for j in range(0, len(b)):
        aligns_mat[0, j] = "←"

    for i in range(0, len(a)):
        aligns_mat[i, 0] = "↑"
    aligns_mat[0,0] = 'x'

    # Fill matrices
    for i in range(1, len(a)):
        for j in range(1, len(b)):

            best_score, best_edit, best_align = get_score(a, b, i, j, scores_mat, edits_mat, letter_scores)
            scores_mat[i, j] = best_score
            edits_mat[i, j] = best_edit
            aligns_mat[i, j] = best_align

    return scores_mat, edits_mat, aligns_mat


# ---------------------------------------------------------------------
def get_alignment(a: str, b: str, edits_mat: np.ndarray, aligns_mat: np.ndarray) -> (str, str, str, np.ndarray):

    # Initialize return vars
    path_mat = np.copy(aligns_mat)
    edits = ""
    a_alignment = ""
    b_alignment = ""

    # Initialize loop vars
    start_pos = (len(a) - 1, len(b) - 1)
    end_pos = (0, 0)
    i, j = start_pos
    finished = (i, j) == end_pos

    # Loop
    while not finished:
        path_mat[i, j] = 'O'
        edit = edits_mat[i, j]
        edits = edit + edits
        a_letter = a[i]
        b_letter = b[j]
        if edit == "s":
            a_alignment = a_letter + a_alignment
            b_alignment = b_letter + b_alignment
            i = i - 1
            j = j - 1
        elif edit == "d":
            a_alignment = a_letter + a_alignment
            b_alignment = "-" + b_alignment
            i = i - 1
            j = j
        else:
            a_alignment = "-" + a_alignment
            b_alignment = b_letter + b_alignment
            i = i
            j = j - 1
        finished = (i, j) == end_pos

    return a_alignment, b_alignment, edits, path_mat


# ---------------------------------------------------------------------
def print_alignment(a_alignment, b_alignment) -> (str):

    alignment = ""

    for index in range(len(a_alignment)):
        if a_alignment[index] == b_alignment[index]:
            alignment = alignment + "|"
        else:
            alignment = alignment + " "

    return alignment

# ---------------------------------------------------------------------
def print_matrix(a: str, b: str, mat: np.ndarray):
    
    row_names = list(a)
    col_names = list(b)
    df        = pd.DataFrame(mat, index=row_names, columns=col_names)

    print(df)
    print()

# ---------------------------------------------------------------------

