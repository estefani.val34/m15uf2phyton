# - M15-UF2: (Bio)Python Pe1b (2019-12-16)
# ---------------------------------------------------------------------
# - Nom:ESTFENI
# - Cognoms:PAREDES VALERA
# - DNI:23923587E
# ---------------------------------------------------------------------

"""
Problema B: BioPython (6 punts)
---------------------------------------------------------------------
Mira el fitxer "gene.gb".
Conté la seqüència codificant d'un gen, però el començament és ambigu.

Escriu el codi que ens permeti fer les següents tasques:
1. Llegir el fitxer "gene.gb" (no cal utilitzar argparse).
2. Llegir el començament (ambigu) i el final (exacte) de la seqüència codificant.
3. Generar totes les possibles seqüències.
4. Descartar les seqüències que no tenen una longitud múltiple de 3.
5. Traduir les seqüències a proteines.
6. Descartar les seqüències que no comencen per una Metionina.
Finalment, mostra les seqüencies per pantalla.

Exemple:
- Teniu un exemple a "example.txt"

Respostes:
- Escriu tot el teu codi en aquest fitxer: pe1b.py.
- Quan acabis, puja-ho tot al Moodle comprimit en un únic arxiu .zip.

Avaluació:
- Les vostres respostes s'avaluaran executant 'python pe1b.py'
- Tots els fitxers estaran al mateix directori que pe1b.py.
- Assegureu-vos que el vostre codi NO dóna cap error abans d'entregar-lo!

Puntuació:
- Un punt per cada tasca.

Notes:
- Us recomamen utilitzar IPython per fer proves i investigar el SeqRecord.
- Qualsevol Posició de BioPython es pot convertir en un enter directament.
"""


# ---------------------------------------------------------------------

# Write here your code...
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.Alphabet import generic_rna


gb_recs = list(SeqIO.parse("gene.gb", "genbank"))
rec = gb_recs[0]

rec_feat_list = rec.features
rec_seq_feat = rec_feat_list[0]
num_inici_cds = rec_seq_feat.location.start.position

sequence = str(rec.seq)

sequences_list = [sequence[i : i + num_inici_cds] for i in range(0, len(sequence), num_inici_cds)]

seq_inici_ambigu_str = sequences_list[0]
seq_final_exacte_list = sequences_list[1:]

seq_final_exacte_str = ""

for x in seq_final_exacte_list:
    seq_final_exacte_str += x
#    print(x)


lenght = len(seq_inici_ambigu_str)
for i in range(lenght):
    seq = Seq(
        seq_inici_ambigu_str[i] + seq_inici_ambigu_str[i + 1 :] + seq_final_exacte_str,
        generic_rna,
    )
    lenseq = len(seq)
    if lenseq % 3 == 0 and str(seq.translate())[0] == "M":
        print(seq.translate())
# ---------------------------------------------------------------------

