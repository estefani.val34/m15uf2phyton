"""
Generate BED
"""
import argparse
import sys
import re
from Bio import SeqIO
#from Bio import SeqFeature

PARSER = argparse.ArgumentParser()
PARSER.add_argument("infile", nargs="?", type=argparse.FileType("r"), default=sys.stdin)
PARSER.add_argument("infile2", nargs="?", type=argparse.FileType("r"), default=sys.stdin)
ARGS = PARSER.parse_args()

#I read txt
with open(ARGS.infile.name, "r") as txt_file:
    TXT = txt_file.readlines()

#I read genbank
RECS = list(SeqIO.parse(ARGS.infile2.name, "genbank"))
REC = RECS[0]

#Generate BED. As I get the results I will be adding to the Track array

BED = []# the final array that concatenates the other arrays: TRACK.*

#TRACK_STR stores: chromosome number, start and end of the gene, the track (name, num, plus),
#start and end of the CDS, visibility and number of blocks
TRACK_STR = ["chr"]

TRACK_END_SIZE = []#the array stores  size of each exon
TRACK_END_RELATIVE = []#the array stores relative star of each exon

#Variables features
FEAT_SOURCE = '' #REC.features[0], to obtain chromosome number
FEAT_CDS = '' #REC.features[5], to obtain CDS

#DIC_NAMES_EXONS I get the names of each exon
DIC_NAMES_EXONS = {"start_exon": "", "end_exon": "", "relative_start_exon": "", "size_exon": ""}
DIC_EXONS = {}# generate as many names as exons there are, to obtain the position of each exon
NUM_EXONS = 0
i = 0

for i in range(len(REC.features)):
    if REC.features[i].type == "source":
        FEAT_SOURCE = REC.features[i]
    if REC.features[i].type == "CDS":
        FEAT_CDS = REC.features[i]
    if REC.features[i].type == "exon":
        NUM_EXONS = NUM_EXONS + 1
        for x, y in DIC_NAMES_EXONS.items():
            DIC_EXONS[x + str(NUM_EXONS)] = ""
        #REC.features[i] feature of each exon
        DIC_EXONS['start_exon'+str(NUM_EXONS)] = REC.features[i].location.start.position
        DIC_EXONS['end_exon'+str(NUM_EXONS)] = REC.features[i].location.end.position
        start_each_exon = DIC_EXONS['start_exon'+str(NUM_EXONS)]
        # I get relative start of eaxh exon
        DIC_EXONS['relative_start_exon'+str(NUM_EXONS)] = start_each_exon-DIC_EXONS['start_exon1']
        # I get block size
        DIC_EXONS['size_exon'+str(NUM_EXONS)] = DIC_EXONS['end_exon'+str(NUM_EXONS)]-start_each_exon
        TRACK_END_SIZE.append(str(DIC_EXONS['size_exon'+str(NUM_EXONS)]))
        TRACK_END_RELATIVE.append(str(DIC_EXONS['relative_start_exon'+str(NUM_EXONS)]))

# I get the chromosome number
QUALI_DICT = FEAT_SOURCE.qualifiers
SOURCE_MAP_STR = QUALI_DICT.get("map")[0]
REG_CHR = r"(\d{2})p"
PAT = re.compile(REG_CHR)
MATCH = PAT.search(SOURCE_MAP_STR)
NUMBER_CHR = MATCH.group(1)

TRACK_STR[0] = TRACK_STR[0]+str(NUMBER_CHR)


# I get the start of the gene

INDEX = 0
REG = r"\tcurrent\t"
LENGHT_TXT = len(TXT)
for i in range(LENGHT_TXT):
    PAT = re.compile(REG, re.DOTALL)
    MATCH = PAT.search(TXT[i])
    if MATCH != None:
        INDEX = i


REG_NUM = r"(\d+)\.\."
PAT = re.compile(REG_NUM)
MATCH = PAT.search(TXT[INDEX])
START_GEN = int(MATCH.group(1))

TRACK_STR.append(str(START_GEN))

# I get the end of the gene
INDEX_END = int(NUM_EXONS)-1
END_GEN = int(START_GEN)+int(TRACK_END_SIZE[INDEX_END])+int(TRACK_END_RELATIVE[INDEX_END])
TRACK_STR.append(str(END_GEN))

# I get data of the gene
NAME_GEN = "GENE"
TRACK_STR.append(str(NAME_GEN))

NUM_GEN = "1"
TRACK_STR.append(str(NUM_GEN))

PLUS_GEN = "-"
TRACK_STR.append(str(PLUS_GEN))

# I get CDS
CDS_START = int(FEAT_CDS.location.start)
CDS_END = int(FEAT_CDS.location.end)

# CDS start
CDS_START_ABS = (CDS_START - DIC_EXONS['start_exon1']) + START_GEN
TRACK_STR.append(str(CDS_START_ABS))
# CDS end
CDS_END_ABS = (CDS_END - DIC_EXONS['start_exon1']) + START_GEN
TRACK_STR.append(str(CDS_END_ABS))

# I get visibility color
VISIBILITY_COLOR = 0
TRACK_STR.append(str(VISIBILITY_COLOR))

# I get number of blocks =  number of exons
TRACK_STR.append(str(NUM_EXONS))
# I get BED
print('BED Array :')
BED = TRACK_STR+TRACK_END_SIZE+TRACK_END_RELATIVE
print(BED)
print('BED String :')
TRACK_END_SIZE_JOIN = ",".join(TRACK_END_SIZE)
TRACK_END_RELATIVE_JOIN = ",".join(TRACK_END_RELATIVE)
BED_JOIN = " ".join(TRACK_STR)
BED_STRING = BED_JOIN+" "+TRACK_END_SIZE_JOIN+" "+TRACK_END_RELATIVE_JOIN
print(BED_STRING)




#regex para txt: Descartar todo lo que no es genomics context

# cada linea es un posicion de la lista
#estrategias  : buscar la linea que tiene la palabra "current"
#estraer las coordenadas
#
#regex: txt, reg, PAT, MATCH
#finditer: devuelve un iterador si encuentra algo
#search: devuelve un MATCH, sino encuentra nada un none
#GRUPOS:  .*
#single line--> como me coga todos los textos incluso os saltos de linea equivale al re.DOTALL
#RE.dOTALL: Coincide con ltodos los caracteres incloso el s123estealto de linea

#reg=r"GENOMIC CONTEXT(.*)BIBLIOGRAPHY"
#PAT=re.compile(reg, re.DOTALL)  # EL PUNTO COINCIDA CON EL SALTO DE LINEA
#MATCH=PAT.search(txt)
#MATCH.group()

#https://www.ncbi.nlm.nih.gov/nuccore/AH002844.2?expand-gaps=on

#For call by bash  =>  python pt2_a.py insulin.txt insulin.gb
