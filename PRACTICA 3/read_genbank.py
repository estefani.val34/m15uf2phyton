import sys
import argparse


PARSER = argparse.ArgumentParser()
PARSER.add_argument('infile', nargs='?', type=argparse.FileType('r'), default=sys.stdin)
ARGS = PARSER.parse_args()

F = open(ARGS.infile.name, "r")
TXT = F.read()


print(TXT)
F.close()
