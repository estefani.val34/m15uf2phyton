"""
Generate BED
python pt2_b.py cccD 5
"""
import argparse
from Bio import Entrez

Entrez.email = "estefani.val34@gmail.com"

PARSER = argparse.ArgumentParser()
PARSER.add_argument("TERM_SEARCH")
PARSER.add_argument("MAX_SEARCH")
ARGS = PARSER.parse_args()

TERM_SEARCH = ARGS.TERM_SEARCH
MAX_SEARCH = int(ARGS.MAX_SEARCH)

#"opuntia[ORGN] accD"
with Entrez.esearch(db="nucleotide",
                    term=TERM_SEARCH,
                    idtype="acc",
                    retmax=MAX_SEARCH) as response:
    RES = Entrez.read(response)

ID = ",".join(RES["IdList"])
with Entrez.epost("nucleotide", id=ID) as response:
    EPOST = Entrez.read(response)

QUERY_KEY = EPOST['QueryKey']

WEBENV = EPOST['WebEnv']

ACC_ARR = RES['IdList']
ACC_ARR2 = ACC_ARR.copy()

with Entrez.efetch(db="nucleotide",
                   retmode="text",
                   rettype="gb",
                   retstart=0,
                   retmax=1,
                   webenv=WEBENV,
                   query_key=QUERY_KEY,
                   idtype="acc") as response:
    ACC_ARR[0] = response.read()

LENGHT = len(ACC_ARR)
for i in range(1, LENGHT, 1):
    with Entrez.efetch(db="nucleotide",
                       retmode="text",
                       rettype="gb",
                       retstart=i,
                       retmax=i,
                       webenv=WEBENV,
                       query_key=QUERY_KEY,
                       idtype="acc") as response:
        ACC_ARR[i] = response.read()

LENGHT2 = len(ACC_ARR2)
for i in range(LENGHT2):
    with open(ACC_ARR2[i]+'.gb', 'w') as gb_file:
        gb_file.write(ACC_ARR[i])
