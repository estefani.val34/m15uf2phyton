#CHAPTER 9 DE BIOPYTHON
from Bio import Entrez # Entrez es una api de NCBI 

Entrez.email = "estefani.val34@gmail.com"  # vueestro correo de verdad

# Entrez usa handelrs: es mas que un iterador, como open("fihcero.txt","r")
# https://www.ncbi.nlm.nih.gov/books/NBK25501/
"""
9.2***einfo: devuelve informacion sobre las bases de datos que hay en el NCBI
"""
#OPTION 1
handle = Entrez.einfo()
type(handle)  # _io.TextIOWrapper : con _ que es algo privado , pensar que es como un fichero
res = handle.read()  # como los iteradores solo se puede leer una vez, no va para atras
# res es el resultado te da un identificador interno , le lanza la peticon al NCBI y ellos nos contestan 
# res es un xsml 
handle.close()  


#OPTION 2, so it closes automatically
with Entrez.einfo() as response :
    dbs_xsml_str=response.read()# es el res, es un string en mi RAM 

"""
escribir un string en disco. Guardar en disco 
"""
with open("dbs.xml","w") as xml_file: # si no existe me lo crea 
    xml_file.write(dbs_xsml_str)

"""
leer de disco el xml y convertirlo a diccionario
"""
with open("dbs.xml","r") as xml_file: 
    dbs_dict=Entrez.read(xml_file) #entrez admite un handle (red, archivo,etc)y lo convierte a un diccionario 


#Convierte el xml a un diccionario (es una objeto que hereda de diccionario )
with Entrez.einfo() as response :
    dbs_dict=Entrez.read(response)#entrez lee responde , lo parsea el responde.read() y me lo convierte a un diccionario de python
    
dbs_dict.keys()
dbs_dict['DbList']# una lista con todas las bases de datos que tiene el NCBI, con estos nombres podemos atacar el NCBI

"""
pretty printy
"""
#pretty printy : imprime bonito 
import pprint

pp= pprint.PrettyPrinter(indent=4) #objeto llamo al contructor y que me indente e 4 espacios 
pp.pprint(dbs_dict)



# Obtener Infomacion sobre una base de datos concreta 

with Entrez.einfo(db="nucleotide") as response : 
  nuc_db=Entrez.read(response) 

type(nuc_db) 
nuc_db.keys() 
nuc_db['DbInfo']   
nuc_db['DbInfo'].keys() 
nuc_db['DbInfo']['DbName'] 
# entradas que hay en la base de datos, las millones de secuencias que hay 
nuc_db['DbInfo']['Count']  
#cuando se actualizo por ultim vez
nuc_db['DbInfo']['LastUpdate']
#descripcion
nuc_db['DbInfo']['Description']  
#la lista de campos especificos con los que yo 
# puedo filtrar mis busquedas en la base de datos 
nuc_db['DbInfo']['FieldList']
#lo hago bonito 
for field in nuc_db['DbInfo']['FieldList']: 
    pp.pprint(field)
    print()

#accD[GENE] Brassica[ORG]: EL ncbi que me busque el gen , me sustituye en GENE por accD, sustituye ORG por Brassica y me lo busca

"""
9.3***ESearch: buscar en bases de datos 
"""


with Entrez.esearch(db="nucleotide", # la base de datos
    term="opuntia[ORGN] accD", # lo que busco
    idtype="acc", #el tipo, me da el identificador Accession numbers =acc 
    retmax=10) as response: #como maximo devuelve las primeros 10 coincidencias
        res = Entrez.read(response)

with Entrez.esearch(db="nucleotide",  term="opuntia[ORGN] accD",  idtype="acc", retmax=10) as response:  
    res = Entrez.read(response)# me devuelve u8n dic en memoria 

pp.pprint(res)


""" 
lo que sale del res
me explica los que ha hecho para hacer la busqueda
{   'Count': '3', los que ha encontrado 
    'IdList': id de cada registro , son los accesso numbers 
    'QueryTranslation': 
    'RetMax': '3', lo maximo que quiero , lo maximo que quiero bajar
    'RetStart': '0', para bajarnos los registros con estos identificadores; el minimo que voy a bajar
    'TranslationSet':  lo que ha traducido 
    'TranslationStack':  me da info como ha hecho la busqueda 
}
"""
# las coincidencias que he tenido
res['Count']  
int(res['Count']) # para convertirlo a int 

"""
9.4***EPost : Upload list IDs 
subir al ncbi la lista de accession numbers porque luego me los voy a bajar, quiero hacer algo con ellos
"""
# me interesa bajarme los documentos del IdList  'IdList': ['EF590893.1', 'EF590892.1', 'HQ620723.1']
res['IdList'] 

with Entrez.epost("nucleotide", # la base de datos
id=",".join(res["IdList"])) #identificadores los une separados por comas : string separado por comas 
as response: 
    epost_xml_str = response.read() 

with Entrez.epost("nucleotide", id=",".join(res["IdList"])) as response: 
    epost = Entrez.read(response)                                                                                 

 pp.pprint(epost)  # me da las peticiones que he pedido algo    
 # con epost_xml_str los subo al servidor, ya se acuerda que esto es la lista que yo he subido para bajarme 
 #1.-obtengo una lista de IDS
 #2.-Los subo al servidor con epost. El servidor me devuelve un ID de esta lista
"""
con esto el ya se acuerda 
 {   'QueryKey': '1',# identifica la lista de accessions numbers que as subido
    'WebEnv': 'NCID_1_141034006_130.14.22.76_9001_1575317297_618884994_0MetA0_S_MegaStore'}
"""
 # 3.-Decirle al servido que me envie los ficheros de la  'QueryKey' 'WebEnv'

"""
9.6***Efect: la funcion que me permite bajarme los archivos
"""
query_key=epost['QueryKey'] # identificador de la lista que me quiero bajar
webenv=epost['WebEnv'] 

# vamos a bajarnos el gebank de cada id

with Entrez.efetch( db="nucleotide",# dase de datos
                    retmode="text",# modo de fichero xml, json, txt que es text (fasta, gb)
                    rettype="gb",# el tipo concreto, la abreviatura
                    retstart=0,# a partir de que indice quiero que me empieze a bajar, como tengo 3 accesion number le digo que empieze por el 0
                    retmax=3,# numero total de ficheros que se va a bajar
                    webenv=webenv,# del epost
                    query_key=query_key,# dep epost
                    idtype="acc") # son accession numbers
                    as response:
    gb_txt = response.read()

print(gb_txt)
# guardar en disco
with open("prueba.gb","w") as gb_file: 
    gb_file.write(gb_txt)




#res_dct={}                                                                                                                                                                   

#for i in range(0,len(acc_arr),1): 
#    res_dct[acc_arr[i]]='' 
    

#for x in res_dct.keys(): 
#    for i in range(1,len(acc_arr),1): 
#       res_dct[acc_arr[i]]=i 


acc_arr=res['IdList']  
acc_arr2=acc_arr.copy()  

with Entrez.efetch(db="nucleotide",retmode="text",rettype="gb",retstart=0,retmax=1,webenv=webenv,query_key=query_key,idtype="acc" ) as response: 
    acc_arr[0]=response.read() 
for i in range(1,len(acc_arr),1): 
    with Entrez.efetch(db="nucleotide",retmode="text",rettype="gb",retstart=i,retmax=i,webenv=webenv,query_key=query_key,idtype="acc" ) as response:  
        acc_arr[i]=response.read()  

for i in range(len(acc_arr2)):
    with open(acc_arr2[i]+'.gb', 'w') as gb_file:
        gb_file.write(acc_arr[i])