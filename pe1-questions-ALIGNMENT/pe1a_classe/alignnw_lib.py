import numpy as np

SAME_LETTER = 3
DIFF_LETTER = -3
GAP_PENALTY = -2
DELETION_PENALTY = -2
INSERTION_PENALTY = -2


def get_similarity(a_letter: str, b_letter: str, subst_mat) -> (int):

    letters = list("ATGC")
    a_index = letters.index(a_letter)
    b_index = letters.index(b_letter)
    
    return subst_mat[a_index, b_index]


def get_score(a: str, b: str, i: int, j: int, scores_mat: np.ndarray, subst_mat: np.ndarray)\
    -> (int, int, int):
    substitution_score = scores_mat[i - 1, j - 1] + get_similarity(a[i], b[j], subst_mat)
    deletion_score = scores_mat[i - 1, j] + DELETION_PENALTY
    insertion_score = scores_mat[i, j - 1] + INSERTION_PENALTY
    scores = [substitution_score, deletion_score, insertion_score]
    edits = ["s", "d", "i"]
    aligns = ["↖", "↑", "←"]
    max_score = max(scores)
    max_score_index = scores.index(max_score)
    best_score = max_score
    best_edit = edits[max_score_index]
    best_align = aligns[max_score_index]
    return best_score, best_edit, best_align


def create_mats(a: str, b: str, subst_mat: np.ndarray) \
    -> (np.ndarray, np.ndarray, np.ndarray):

    scores_mat = np.zeros((len(a), len(b)), int)
    for j in range(0, len(b)):
        scores_mat[0, j] = INSERTION_PENALTY * j
    for i in range(0, len(a)):
        scores_mat[i, 0] = DELETION_PENALTY * i

    edits_mat = np.zeros((len(a), len(b)), str)
    for j in range(0, len(b)):
        edits_mat[0, j] = "i"
    for i in range(0, len(a)):
        edits_mat[i, 0] = "d"

    aligns_mat = np.zeros((len(a), len(b)), str)
    for j in range(0, len(b)):
        aligns_mat[0, j] = "←"
    for i in range(0, len(a)):
        aligns_mat[i, 0] = "↑"

    for i in range(1, len(a)):
        for j in range(1, len(b)):
            best_score, best_edit, best_align = get_score(a, b, i, j, scores_mat, subst_mat)
            scores_mat[i, j] = best_score
            edits_mat[i, j] = best_edit
            aligns_mat[i, j] = best_align
    return scores_mat, edits_mat, aligns_mat


def get_alignment(a: str, b: str, edits_mat: np.ndarray, aligns_mat: np.ndarray) -> (str, str, str, np.ndarray):
    path_mat = np.copy(aligns_mat)
    edits = ""
    a_alignment = ""
    b_alignment = ""
    start_pos = (len(a) - 1, len(b) - 1)
    end_pos = (0, 0)
    i, j = start_pos
    finished = (i, j) == end_pos
    while not finished:
        path_mat[i, j] = 0
        edit = edits_mat[i, j]
        edits = edit + edits
        a_letter = a[i]
        b_letter = b[j]
        if edit == "s":
            a_alignment = a_letter + a_alignment
            b_alignment = b_letter + b_alignment
            i = i - 1
            j = j - 1
        elif edit == "d":
            a_alignment = a_letter + a_alignment
            b_alignment = "-" + b_alignment
            i = i - 1
            j = j
        else:
            a_alignment = "-" + a_alignment
            b_alignment = b_letter + b_alignment
            i = i
            j = j - 1
        finished = (i, j) == end_pos

    return a_alignment, b_alignment, edits, path_mat


def print_alignment(a_alignment, b_alignment) -> (str):
    alignment = ""
    for iter in range(len(a_alignment)):
        if a_alignment[iter] is b_alignment[iter]:
            alignment = alignment + "|"
        else:
            alignment = alignment + " "
    return alignment
