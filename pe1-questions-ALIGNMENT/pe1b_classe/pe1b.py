# - M15-UF2: (Bio)Python Pe1b (2019-12-09)
# ---------------------------------------------------------------------
# - Nom:
# - Cognoms:
# - DNI:
# ---------------------------------------------------------------------
# - Enunciat:
#   Escriu el codi necessari per resoldre el problema B en aquest fitxer.
#   Quan acabis, puja-ho tot al Moodle comprimit en un únic arxiu .zip.
#
# - IMPORTANT:
#   Les vostres respostes s'avaluaran executant 'python3 pe1b.py'.
#   El fitxer 'experiment.data' estarà al mateix directori que pe1b.py.
#   Assegureu-vos que el vostre codi NO dóna cap error abans d'entregar-lo!
# ---------------------------------------------------------------------


# Problema B: Biopython (4.0 punts)
# ---------------------------------------------------------------------
# - El fitxer "seqs.fasta" conté dues seqüències que son dos exons d'un gen.
# - Llegeix el fitxer fasta i guarda'l com a un genbank anomenat "out.gb"
# - L'arxiu genbank ha de contenir els dos exons com a "features".
#   Tens un exemple a "example.gb".

# - Notes:
#   - Has d'utilitzar SeqIO per fer la lectura i l'escriptura.
#   - Les localitzacions dels exons han de ser les del fitxer fasta.
#   - El accession number no està al fitxer fasta, però pots utilitzar "TEST_0001.1".

# - Codi:
#   - Es recomana utilitzar el següent import:
#     from Bio.SeqFeature import SeqFeature, FeatureLocation

# - Puntuació:
#   - Llegir i gravar: 1 punt
#   - Seqüència: 1 punt
#   - Exon1: 1 punt
#   - Exon2: 1 punt


# Imports
# ---------------------------------------------------------------------
import re

from Bio import SeqIO
from Bio.Seq import Seq
from Bio.Alphabet import IUPAC
from Bio.SeqRecord import SeqRecord
from Bio.SeqFeature import SeqFeature, FeatureLocation


# ---------------------------------------------------------------------

# Read fasta
fasta_recs = list(SeqIO.parse("seqs.fasta", "fasta", IUPAC.unambiguous_dna))


# Get exon 1 info
exon1_rec  = fasta_recs[0]
exon1_seq  = exon1_rec.seq
exon1_desc = exon1_rec.description

# Get exon 1 start, end
reg   = r'(\d+)\.\.(\d+)'
pat   = re.compile(reg)
match = pat.search(exon1_desc)
exon1_start = int(match.group(1))
exon1_end   = int(match.group(2))


# Get exon 2 info
exon2_rec  = fasta_recs[1]
exon2_seq  = exon2_rec.seq
exon2_desc = exon2_rec.description

# Get exon 2 start, end
# Es pot fer igual que amb el exon1.
# Aquí ho faig diferent per tal que veieu com es podria fer usant .finditer()
reg     = r'\d+'
pat     = re.compile(reg)
matches = list(pat.finditer(exon2_desc))
start_match = matches[0]
end_match   = matches[1]
exon2_start = int(start_match.group(0))
exon2_end   = int(end_match.group(0))


# Create SeqRecord
rec = SeqRecord(exon1_seq + exon2_seq, id="TEST_0001.1")

# Create SeqFeatures
exon1_loc = FeatureLocation(exon1_start, exon1_end)
exon2_loc = FeatureLocation(exon2_start, exon2_end)

exon1_feat = SeqFeature(exon1_loc, type="exon")
exon2_feat = SeqFeature(exon2_loc, type="exon")

# Add SeqFeatures to SeqRecord
rec.features.append(exon1_feat)
rec.features.append(exon2_feat)

# Write SeqRecord
SeqIO.write(rec, "out.gb", "gb")

# ---------------------------------------------------------------------

