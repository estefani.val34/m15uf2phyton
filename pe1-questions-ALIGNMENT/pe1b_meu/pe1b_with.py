# - M15-UF2: (Bio)Python Pe1b (2019-12-09)
# ---------------------------------------------------------------------
# - Nom:
# - Cognoms:
# - DNI:
# ---------------------------------------------------------------------
# - Enunciat:
#   Escriu el codi necessari per resoldre el problema B en aquest fitxer.
#   Quan acabis, puja-ho tot al Moodle comprimit en un únic arxiu .zip.
#   fasta a genbank
# - IMPORTANT:
#   Les vostres respostes s'avaluaran executant 'python3 pe1b.py'.
#   El fitxer 'experiment.data' estarà al mateix directori que pe1b.py.
#   Assegureu-vos que el vostre codi NO dóna cap error abans d'entregar-lo!
# ---------------------------------------------------------------------


# Problema B: Biopython (4.0 punts)
# ---------------------------------------------------------------------
# - El fitxer "seqs.fasta" conté dues seqüències que son dos exons d'un gen.
# - Llegeix el fitxer fasta i guarda'l com a un genbank anomenat "out.gb"
# - L'arxiu genbank ha de contenir els dos exons com a "features".
#   Tens un exemple a "example.gb".

# - Notes:
#   - Has d'utilitzar SeqIO per fer la lectura i l'escriptura.
#   - Les localitzacions dels exons han de ser les del fitxer fasta.
#   - El accession number no està al fitxer fasta, però pots utilitzar "TEST_0001.1".

# - Codi:
#   - Es recomana utilitzar el següent import:
#     from Bio.SeqFeature import SeqFeature, FeatureLocation

# - Puntuació:
#   - Llegir i gravar: 1 punt
#   - Seqüència: 1 punt
#   - Exon1: 1 punt
#   - Exon2: 1 punt

# input shell: python pe1b.py

# Codi:
# ---------------------------------------------------------------------
import re
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio import SeqIO
from Bio.Alphabet import generic_dna
from Bio.SeqFeature import SeqFeature, FeatureLocation

#record=SeqIO.parse("seqs.fasta", "fasta" , IUPAC.UNAMBIGUIS_DNA)
#SeqIO.write(ot_rec, "seqs.gb", "gb")
with open("seqs.fasta", "r") as input_handle:
    with open("out.gb", "w") as output_handle:

        # input_handle = open("seqs.fasta", "r")
        # output_handle = open("seqs.gb", "w")

        sequences = list(SeqIO.parse(input_handle, "fasta"))
        list_seqRecord = []
        list_seqFeatures = []
        concatenatedseq = Seq("", generic_dna)
        id_name_str = "TEST_0001.1"

        regexon = r"(exon)"
        pat = re.compile(regexon)
        regexpos = r"(\d)+\.\.(\d+)"
        pat2 = re.compile(regexpos)

        for seq in sequences:
            concatenatedseq += seq.seq
            match = pat.search(seq.description)
            match2 = pat2.search(seq.description)
            list_seqFeatures.append(
                SeqFeature(
                    FeatureLocation(int(match2.group(1)), int(match2.group(2))),
                    type=match.group(1),
                )
            )

        record = SeqRecord(concatenatedseq)
        record.seq.alphabet = generic_dna
        record.id = id_name_str
        record.features = list_seqFeatures
        list_seqRecord.append(record)

        SeqIO.write(list_seqRecord, output_handle, "genbank")

# output_handle.close()
# input_handle.close()
# ---------------------------------------------------------------------

# match=pat.search("gene TEST, exon, 1..42")
# match_type=match.group(1)
# match2=pat2.search("gene TEST, exon, 1..42")
# match2.group(1)
# match2.group(2)
# f1 = SeqFeature(FeatureLocation(4, 10), type="exon")
# f2 = SeqFeature(FeatureLocation(5, 13), type="exon")
# list_seqFeatures.append(f1)
# list_seqFeatures.append(f2)
