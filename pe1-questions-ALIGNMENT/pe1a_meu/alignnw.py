# - M15-UF2: (Bio)Python Pe1a (2019-12-09)
# ---------------------------------------------------------------------
# - Nom:
# - Cognoms:
# - DNI:
# ---------------------------------------------------------------------
# - Enunciat:
#   - Escriu el codi necessari per resoldre el problema A
#   - modificant els fitxers 'alignnw.py' i 'alignnw_lib.py'.
#   Quan acabis, puja-ho tot al Moodle comprimit en un únic arxiu .zip.
#
# - IMPORTANT:
#   - Les vostres respostes s'avaluaran executant 'python3 alignw.py'.
#   - Tots els fitxers estaran al mateix directori que alignw.py.
#   - Assegureu-vos que el vostre codi NO dóna cap error abans d'entregar-lo!
# ---------------------------------------------------------------------

import alignnw_lib as nw
import argparse
import sys
import re
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument("a")
parser.add_argument("b")
parser.add_argument("infile", nargs="?", type=argparse.FileType("r"), default=sys.stdin)
args = parser.parse_args()

a = " " + args.a.upper()
b = " " + args.b.upper()
with open(args.infile.name, "r") as mat_file:
    mat_txt = mat_file.read()

print("seq1: " + a)
print("seq2: " + b)
print("mat_txt:\n " + mat_txt)
m=[]
regex=r"(-?\d)" 
PAT = re.compile(regex, re.MULTILINE)
for match in re.finditer(PAT, mat_txt):
    m.append(int(match.group()))

mat=np.array(m)    
mat.shape = (4,4)
#print(mat)

scores_mat, edits_mat, aligns_mat = nw.create_mats(a, b,mat)

print(scores_mat)
print(edits_mat)
print(aligns_mat)
print("MATRIX: ")
print(scores_mat)

a_alignment, b_alignment, edits, path_mat = nw.get_alignment(a, b, edits_mat, aligns_mat)
alignment = nw.print_alignment(a_alignment, b_alignment)

print("ALIGNMENT:\n" + a_alignment)
print(alignment)
print(b_alignment)

# ---------------------------------------------------------------------

