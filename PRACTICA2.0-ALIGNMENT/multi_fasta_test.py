"""Fasta"""


import re


import local_Smith_Waterman.alignsw_lib
import global_Needleman_Wunsch.alignnw_lib



LIST_SEQ2 = []

SECUENCIA1 = "AGCGTGGT"

KEY_VALUE = {}

F = open("secuencia.fasta", "r")

TXT = F.read()
REGEX = r'^>.*[^>]'

PAT = re.compile(REGEX, re.MULTILINE)

TXT_SUB = PAT.sub("*", TXT)
LIST_TXT_SPLIT = TXT_SUB.split("*")
LENGHT = len(LIST_TXT_SPLIT)

for i in range(LENGHT):
    if LIST_TXT_SPLIT[i] != '':
        LIST_SEQ2.append(LIST_TXT_SPLIT[i].replace('\n', ''))

F.close()


for x in LIST_SEQ2:
    SECUENCIA2 = x
    M = local_Smith_Waterman.alignsw_lib.get_matrix(SECUENCIA1, SECUENCIA2)
    info_score = local_Smith_Waterman.alignsw_lib.get_max_score(M)
    KEY_VALUE[info_score[0]] = [SECUENCIA2, "SW"]

    M2 = global_Needleman_Wunsch.alignnw_lib.get_matrix(SECUENCIA1, SECUENCIA2)
    score = global_Needleman_Wunsch.alignnw_lib.get_max_score(M2)
    KEY_VALUE[score] = [SECUENCIA2, "NW"]
for i in sorted(KEY_VALUE, reverse=True):
    print("SECUENCIA1=", SECUENCIA1, " SECUENCIA2=", KEY_VALUE[i][0], "   Score=", i, "  OPTION=", KEY_VALUE[i][1])
   