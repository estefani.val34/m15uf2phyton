#1.-Implementar alineamiento local 
#2.-IMplementar alinamiento global
#3.-Implementar algo con regex (pendiente de hacer)
#USAR:
#   -git  ( dos directorios , con dos git separados)
#   -pep8 (obligatorio, foramt document )
#   -argparse
#   -pytest


#1.-Se piden 3 archivos :  algoritmo de Smith-Waterman LOCAL (buscamos la puntiacion mas alta)
#   alignsw.py   ( dos cadenas de adn que queremos alinear, usar argparse-->tiene que salir la ayuda, llamt a las funciones que estan en  alignsw_lib.py )
#   alignsw_lib.py (nuestro codigo que alinea , las 3 funciones )
#   alignsw_test.py  (donde estan nuestros test, a de salir igual el ejemplo de wikipedia, comprovará las 3 funciones por separado)
#   Funciones:
#    a, b son strings
#       get-matrix(a,b)--> matriz   (calcular )  def get_matrix(a:str, b:str)-->np.ndarray:
#       get-score(matriz)---->int  (devuelve un entero del numero de alineamiento )
#       get.alignment(str,str,mat,int)--->[str,str,srt]   (da el elinemaiemto  )
#         EJEMPLO de lo que devuelve : 3 strings 
#              1.string     GATA-CAT
#              2.string     || |   |     (espacios y pibs)
#              3.-string    GA-ATATT                        
#   usar ejemplo en wikipedia


#2.-Se piden 3 archivos : algoritmo de Needleman-Wunsch   GLOBAL (la puntucaion mas alta ya la sebemos )
#   alignnm.py   ( dos cadenas de adn que queremos alinear, usar argparse-->tiene que salir la ayuda)
#   alignnw_lib.py (nuestro codigo que alinea , las 3 funciones )
#   alignnw_test.py  (donde estan nuestros test, a de salir igual el ejemplo de wikipedia)
#   Funciones:
#       get-matrix(str,str)--> matriz   (calcular )
#       get-score(matriz)---->int  (devuelve un entero del numero de alineamiento )
#       get.alignment(str,str,mat,int)--->[str,str,srt]   (da el elinemaiemto  )
#
#   usar ejemplo en wikipedia


#MATRIZ EN PYTHON
# Es un array de arrays 
#numpy: libreria para poder hacer arrats  matriz[2,0]???
# Hi,j --> H:matriz, i:fila(horizontal) , j:columna (vertical) 
#
#1= coincidencia
#else= 0



#Smith–Waterman algorithm
#https://en.wikipedia.org/wiki/Smith%E2%80%93Waterman_algorithm
#

#Needleman–Wunsch algorithm
#https://en.wikipedia.org/wiki/Needleman%E2%80%93Wunsch_algorithm


#instalar la libreria  numpy : es una libreria de matrices en  muy optimizada 
#conda install -n bio -c anaconda numpy
#
#https://www.scipy.org/ --> donde hay una libreria para hacer grafricas (scipy), que se basa en numpy
#
#ipython
#import numpy as np : as es el alias se llamara np
#np. TABULADOR 
#np.zeros  : devuelve una matriz de ceros 
#np.zeros ((3,4), int)
#
#In [2]: np.zeros((3,4),int)                                                                          
#Out[2]: 
#array([[0, 0, 0, 0],
#       [0, 0, 0, 0],
#       [0, 0, 0, 0]])

#In [3]: np.zeros((3,4),np.int)                                                                       
#Out[3]: 
#array([[0, 0, 0, 0],
#      [0, 0, 0, 0],
#      [0, 0, 0, 0]])
#type(mat)--> numpy.ndarray , nd array de n dimesiones

#def get_matrix(a:str, b:str)-->np.ndarray:
